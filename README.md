# MagniTune

MagniTune is an Android app on which you can listen to Croatian radio stations. You can listen to radio stations and save them to your favourite list. App supports playing radio station in the background. To use this app, you need to sign up using built-in signing up form.

To download .apk file, click [here](https://gitlab.com/zagi031/magnitune/-/blob/main/magnitune.apk).

Technologies used: Android, Kotlin, RecyclerView, Firebase services (Authentication, Realtime database), Koin, Navigation Component, Coroutines
