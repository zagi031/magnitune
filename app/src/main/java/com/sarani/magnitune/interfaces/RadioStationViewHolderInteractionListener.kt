package com.sarani.magnitune.interfaces

import com.sarani.magnitune.models.RadioStation

interface RadioStationViewHolderInteractionListener {
    fun onClick(position: Int)

    fun onFavoriteIconClick(radioStation: RadioStation)
}