package com.sarani.magnitune.models.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.yamb.ui.SingleLiveEvent
import com.google.firebase.auth.FirebaseAuth
import com.sarani.magnitune.data.User

class AuthenticationRepository {

    private val auth: FirebaseAuth = FirebaseAuth.getInstance()

    private val _currentUser = MutableLiveData<User>()
    val currentUser: LiveData<User> = _currentUser

    private val _errorMessage = SingleLiveEvent<String>()
    val errorMessage: LiveData<String> = _errorMessage

    fun logIn(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                _currentUser.postValue(
                    User(
                        auth.currentUser!!.providerData[0].uid,
                        task.result?.user!!.providerData[0].email!!
                    )
                )
            } else {
                _errorMessage.postValue(task.exception!!.message)
            }
        }
    }

    fun signUp(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                _currentUser.postValue(
                    User(
                        task.result?.user!!.providerData[0].uid,
                        task.result?.user!!.providerData[0].email!!
                    )
                )
            } else {
                _errorMessage.postValue(task.exception!!.message)
            }
        }
    }

    fun signOut() {
        auth.signOut()
    }

    fun getCurrentUser(): User? {
        if (auth.currentUser != null) {
            val currentUser = User(
                auth.currentUser!!.providerData[0].uid,
                auth.currentUser!!.providerData[0].email!!
            )
            _currentUser.postValue(
                currentUser
            )
            return currentUser
        }
        return null
    }
}