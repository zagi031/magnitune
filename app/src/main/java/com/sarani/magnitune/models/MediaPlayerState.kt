package com.sarani.magnitune.models

import com.sarani.magnitune.R

enum class MediaPlayerState(val iconResId: Int) {
    Playing(R.drawable.ic_baseline_stop_24), Stopped(R.drawable.ic_baseline_play_circle_24)
}