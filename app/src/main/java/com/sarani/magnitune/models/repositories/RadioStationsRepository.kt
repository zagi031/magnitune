package com.sarani.magnitune.models.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.yamb.ui.SingleLiveEvent
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.sarani.magnitune.data.User
import com.sarani.magnitune.models.RadioStation

class RadioStationsRepository {
    private val dbRef = FirebaseDatabase.getInstance().reference

    private val _radioStations = MutableLiveData<List<RadioStation>>()
    val radioStations: LiveData<List<RadioStation>> = _radioStations

    private val _currentUserFavourites = MutableLiveData<List<RadioStation>>()
    val currentUserFavourites: LiveData<List<RadioStation>> = _currentUserFavourites

    private val _toast = SingleLiveEvent<String>()
    val toast: LiveData<String> = _toast

    fun getRadioStations(currentUserUid: String) {
        try {
            dbRef.child("radioStations").addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val radioStations = mutableListOf<RadioStation>()
                    snapshot.children.forEach {
                        val x = it.getValue(RadioStation::class.java)!!
                        x.currentUserFavourite = it.child("users").child(currentUserUid).exists()
                        x.id = it.key!!
                        radioStations.add(x)
                    }
                    _radioStations.postValue(radioStations)
                }

                override fun onCancelled(error: DatabaseError) {
                    _toast.postValue(error.message)
                }

            })
        } catch (e: Exception) {
            _toast.postValue(e.message)
        }
    }

    fun getCurrentUserFavoriteRadioStations(currentUserUid: String) {
        dbRef.child("radioStations")
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val radioStations = mutableListOf<RadioStation>()
                    snapshot.children.filter {
                        it.child("users").children.any { user -> user.key == currentUserUid }
                    }.forEach {
                        val x = it.getValue(RadioStation::class.java)!!
                        x.currentUserFavourite = true
                        x.id = it.key!!
                        radioStations.add(x)
                    }
                    _currentUserFavourites.postValue(radioStations)
                }

                override fun onCancelled(error: DatabaseError) {
                    _toast.postValue(error.message)
                }

            })
    }

    fun addStationToFavourite(radioStation: RadioStation, user: User) {
        dbRef.child("radioStations").child(radioStation.id).child("users").child(user.uid).setValue(user.email)
    }

    fun removeStationFromFavourites(radioStation: RadioStation, user: User) {
        dbRef.child("radioStations").child(radioStation.id).child("users").child(user.uid).removeValue()
    }
}