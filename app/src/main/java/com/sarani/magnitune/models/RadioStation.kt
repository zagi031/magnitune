package com.sarani.magnitune.models

data class RadioStation(
    var id: String,
    val streamURL: String,
    val title: String,
    val imageURL: String,
    var currentUserFavourite: Boolean
) {
    constructor() : this("", "", "", "", false)
    //Firebase needs an empty constructor to be able to deserialize the objects
}
