package com.sarani.magnitune.utils

import android.app.Activity
import android.app.AlertDialog

class ErrorUtil {

    companion object {
        fun showAlertMessage(message: String, activity: Activity) {
            val alertDialog: AlertDialog = activity.let {
                val builder = AlertDialog.Builder(it)
                builder.apply {
                    setPositiveButton("OK"
                    ) { _, _ ->
                    }
                }
                builder.setMessage(message).create()
            }

            alertDialog.show()
        }
    }
}