package com.sarani.magnitune.di

import com.sarani.magnitune.models.repositories.AuthenticationRepository
import com.sarani.magnitune.ui.radioStationsFragment.RadioStationsViewModel
import com.sarani.magnitune.models.repositories.RadioStationsRepository
import com.sarani.magnitune.ui.AuthenticationViewModel
import com.sarani.magnitune.ui.audioPlayerFragment.AudioPlayerViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { RadioStationsViewModel(androidApplication(), get()) }
    viewModel { AudioPlayerViewModel(androidApplication()) }
    viewModel { AuthenticationViewModel(androidApplication(), get()) }

    factory { RadioStationsRepository() }
    factory { AuthenticationRepository() }
}