package com.sarani.magnitune

import android.app.Application
import com.sarani.magnitune.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MagniTune : Application() {

    companion object {
        lateinit var application: MagniTune
    }

    override fun onCreate() {
        super.onCreate()
        application = this
        startKoin {
            androidContext(this@MagniTune)
            modules(viewModelModule)
        }
    }
}