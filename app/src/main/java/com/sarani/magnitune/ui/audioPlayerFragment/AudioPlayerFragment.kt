package com.sarani.magnitune.ui.audioPlayerFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.sarani.magnitune.R
import com.sarani.magnitune.databinding.FragmentAudioPlayerBinding
import com.sarani.magnitune.models.RadioStation
import com.sarani.magnitune.ui.AudioPlayerSharedViewModel
import com.squareup.picasso.Picasso
import org.koin.androidx.viewmodel.ext.android.viewModel

class AudioPlayerFragment : Fragment() {
    private lateinit var binding: FragmentAudioPlayerBinding
    private val viewModel by viewModel<AudioPlayerViewModel>()
    private val sharedViewModel by activityViewModels<AudioPlayerSharedViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAudioPlayerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            viewModel.toast.observe(viewLifecycleOwner, {
                showToast(it)
            })
            sharedViewModel.currentRadioStation.observe(viewLifecycleOwner, {
                viewModel.playRadioStation(it)
                updateCurrentRadioStationUI(it)
                updateIBPlayPauseDrawable()
            })

            ibPlayStop.setOnClickListener {
                viewModel.handleClickOnPlayStopButton()
                YoYo.with(Techniques.FlipInX).playOn(it)
                updateIBPlayPauseDrawable()
            }
            ibSkipPrevious.setOnClickListener {
                animateSkipButton(it, R.anim.slide_and_back_to_the_left)
                sharedViewModel.getPreviousRadioStation()
            }
            ibSkipNext.setOnClickListener {
                animateSkipButton(it, R.anim.slide_and_back_to_the_right)
                sharedViewModel.getNextRadioStation()
            }
        }
    }

    private fun animateSkipButton(view: View, animationID: Int) {
        view.startAnimation(
            AnimationUtils.loadAnimation(
                activity?.baseContext,
                animationID
            )
        )
    }

    private fun updateIBPlayPauseDrawable() =
        binding.ibPlayStop.setImageResource(viewModel.mediaPlayerState.iconResId)

    private fun updateCurrentRadioStationUI(radioStation: RadioStation) {
        binding.apply {
            if (radioStation.imageURL != "") Picasso.get().load(radioStation.imageURL)
                .into(ivStationCover)
            else Picasso.get()
                .load(R.drawable.radiostation_cover_placeholder).into(ivStationCover)
            YoYo.with(Techniques.FadeIn).playOn(ivStationCover)
            tvStationTitle.text = radioStation.title
            YoYo.with(Techniques.FadeIn).playOn(tvStationTitle)
        }
    }

    private fun showToast(message: String) =
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    override fun onDestroy() {
        viewModel.releaseMediaPlayer()
        super.onDestroy()
    }
}