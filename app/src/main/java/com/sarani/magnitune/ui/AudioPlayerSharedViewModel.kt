package com.sarani.magnitune.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.yamb.ui.SingleLiveEvent
import com.sarani.magnitune.models.RadioStation

class AudioPlayerSharedViewModel : ViewModel() {
    private val radioStations = mutableListOf<RadioStation>()
    private var currentRadioStationIndex = -1
    private val _currentRadioStation = SingleLiveEvent<RadioStation>()
    val currentRadioStation: LiveData<RadioStation> = _currentRadioStation
    private val _isMediaPlayerReady = SingleLiveEvent<Boolean>()
    val isMediaPlayerReady: LiveData<Boolean> = _isMediaPlayerReady

    fun setRadioStation(radioStations: List<RadioStation>, index: Int) {
        if (currentRadioStationIndex == -1) {
            _isMediaPlayerReady.postValue(true)
        }
        this.radioStations.clear()
        this.radioStations.addAll(radioStations)
        this.currentRadioStationIndex = index
        this._currentRadioStation.postValue(radioStations[index])
    }

    fun getNextRadioStation() {
        if (currentRadioStationIndex == this.radioStations.size - 1) {
            currentRadioStationIndex = 0
        } else currentRadioStationIndex += 1
        _currentRadioStation.postValue(radioStations[currentRadioStationIndex])
    }

    fun getPreviousRadioStation() {
        if (currentRadioStationIndex == 0) {
            currentRadioStationIndex = this.radioStations.size - 1
        } else currentRadioStationIndex -= 1
        this._currentRadioStation.postValue(radioStations[currentRadioStationIndex])
    }
}