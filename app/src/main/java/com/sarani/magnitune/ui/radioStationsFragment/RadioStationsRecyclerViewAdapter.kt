package com.sarani.magnitune.ui.radioStationsFragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.sarani.magnitune.R
import com.sarani.magnitune.databinding.EmptyRvNoteHolderBinding
import com.sarani.magnitune.databinding.RvRadiostationHolderBinding
import com.sarani.magnitune.interfaces.RadioStationViewHolderInteractionListener
import com.sarani.magnitune.models.RadioStation
import com.squareup.picasso.Picasso

class RadioStationsRecyclerViewAdapter(
    private val listener: RadioStationViewHolderInteractionListener,
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val NON_EMPTY_RECYCLERVIEW_TYPE = 100
        private const val EMPTY_RECYCLERVIEW_TYPE = 200
    }

    private val radioStations = mutableListOf<RadioStation>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == NON_EMPTY_RECYCLERVIEW_TYPE) RadioStationViewHolder(
            RvRadiostationHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        ) else EmptyRecyclerViewNoteHolder(
            EmptyRvNoteHolderBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            )
        )
    }

    override fun getItemViewType(position: Int) =
        if (this.radioStations.isNotEmpty()) NON_EMPTY_RECYCLERVIEW_TYPE else EMPTY_RECYCLERVIEW_TYPE

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == NON_EMPTY_RECYCLERVIEW_TYPE) {
            (holder as RadioStationViewHolder).bind(radioStations[position], listener)
        } else (holder as EmptyRecyclerViewNoteHolder).bind(holder.itemView.context.getString(R.string.label_no_favorite_radio_stations))
    }

    override fun getItemCount() = if (radioStations.isNotEmpty()) radioStations.size else 1

    override fun getItemId(position: Int) = position.toLong()

    fun setRadioStations(radioStations: List<RadioStation>) {
        this.radioStations.clear()
        this.radioStations.addAll(radioStations)
        this.notifyDataSetChanged()
    }

    fun getAllRadioStations(): List<RadioStation> = this.radioStations
}

class RadioStationViewHolder(private val binding: RvRadiostationHolderBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        radioStation: RadioStation,
        listener: RadioStationViewHolderInteractionListener
    ) {
        binding.apply {
            tvRadioStationTitle.text = radioStation.title
            val imgResId =
                if (radioStation.currentUserFavourite) R.drawable.ic_baseline_favorite_on_24 else R.drawable.ic_baseline_favorite_off_24
            ibFavorite.setImageResource(imgResId)
            if (radioStation.imageURL != "") {
                Picasso.get().load(radioStation.imageURL)
                    .placeholder(R.drawable.radiostation_cover_placeholder)
                    .error(R.drawable.radiostation_cover_placeholder)
                    .into(ivRadioStationCover)
            } else Picasso.get().load(R.drawable.radiostation_cover_placeholder)
                .placeholder(R.drawable.radiostation_cover_placeholder)
                .into(ivRadioStationCover)

            root.setOnClickListener {
                YoYo.with(Techniques.RubberBand).playOn(it)
                listener.onClick(adapterPosition)
            }

            ibFavorite.setOnClickListener {
                YoYo.with(Techniques.RubberBand).playOn(it)
                listener.onFavoriteIconClick(radioStation)
            }
        }
    }
}

class EmptyRecyclerViewNoteHolder(private val binding: EmptyRvNoteHolderBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(message: String) {
        binding.tvEmptyRvNote.text = message
    }
}