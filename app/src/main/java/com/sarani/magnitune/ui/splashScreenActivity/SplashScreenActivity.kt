package com.sarani.magnitune.ui.splashScreenActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sarani.magnitune.ui.AuthenticationViewModel
import com.sarani.magnitune.ui.loginActivity.LogInActivity
import com.sarani.magnitune.ui.mainActivity.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashScreenActivity : AppCompatActivity() {

    private val authenticationViewModel by viewModel<AuthenticationViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (authenticationViewModel.getCurrentUser() == null) {
            val loginIntent = Intent(this, LogInActivity::class.java)
            startActivity(loginIntent)
        } else {
            startActivity(Intent(this, MainActivity::class.java))
        }
        finish()
    }
}