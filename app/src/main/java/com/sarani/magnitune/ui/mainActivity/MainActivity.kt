package com.sarani.magnitune.ui.mainActivity

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.marginBottom
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.sarani.magnitune.R
import com.sarani.magnitune.databinding.ActivityMainBinding
import com.sarani.magnitune.ui.AudioPlayerSharedViewModel
import com.sarani.magnitune.ui.audioPlayerFragment.AudioPlayerFragment
import com.sarani.magnitune.ui.homeScreenFragment.HomeScreenFragment

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    private val sharedViewModel by viewModels<AudioPlayerSharedViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().apply {
            add(R.id.fcv_main, HomeScreenFragment())
            add(R.id.fcv_bottom_sheet, AudioPlayerFragment())
        }.commit()

        binding.fcvMain.marginBottom
        binding.bottomSheet.visibility = View.GONE
        BottomSheetBehavior.from(binding.bottomSheet).apply {
            peekHeight = resources.getDimensionPixelSize(R.dimen.bottomSheet_peekHeight)
            this.state = BottomSheetBehavior.STATE_COLLAPSED
        }
        sharedViewModel.isMediaPlayerReady.observe(this, {
            binding.bottomSheet.visibility = View.VISIBLE
            YoYo.with(Techniques.SlideInUp).playOn(binding.bottomSheet)
        })
    }
}