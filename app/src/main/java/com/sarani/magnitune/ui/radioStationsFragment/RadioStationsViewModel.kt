package com.sarani.magnitune.ui.radioStationsFragment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.sarani.magnitune.data.User
import com.sarani.magnitune.models.RadioStation
import com.sarani.magnitune.models.repositories.RadioStationsRepository

class RadioStationsViewModel(
    application: Application,
    private val repository: RadioStationsRepository
) :
    AndroidViewModel(application) {
    val radioStations: LiveData<List<RadioStation>> = repository.radioStations
    val currentUserFavourites: LiveData<List<RadioStation>> = repository.currentUserFavourites
    val toast: LiveData<String> = repository.toast

    fun getRadioStations(currentUserUid: String) =
        repository.getRadioStations(currentUserUid)

    fun getCurrentUserFavourites(currentUserUid: String) =
        repository.getCurrentUserFavoriteRadioStations(currentUserUid)

    fun addStationToFavourite(radioStation: RadioStation, user: User) {
        repository.addStationToFavourite(radioStation, user)
    }

    fun removeStationFromFavourites(radioStation: RadioStation, user: User) {
        repository.removeStationFromFavourites(radioStation, user)
    }
}