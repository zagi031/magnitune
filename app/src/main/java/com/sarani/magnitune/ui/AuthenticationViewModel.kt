package com.sarani.magnitune.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.sarani.magnitune.data.User
import com.sarani.magnitune.models.repositories.AuthenticationRepository

class AuthenticationViewModel(
    application: Application,
    private val repository: AuthenticationRepository
) : AndroidViewModel(application) {

    val currentUser: LiveData<User> = repository.currentUser
    val errorMessage: LiveData<String> = repository.errorMessage

    fun logInUser(email: String, password: String) {
        repository.logIn(email, password)
    }

    fun signUp(email: String, password: String) {
        repository.signUp(email, password)
    }

    fun signOut() {
        repository.signOut()
    }

    fun getCurrentUser() = repository.getCurrentUser()
}