package com.sarani.magnitune.ui.radioStationsFragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.sarani.magnitune.MagniTune
import com.sarani.magnitune.databinding.FragmentRadioStationsBinding
import com.sarani.magnitune.interfaces.RadioStationViewHolderInteractionListener
import com.sarani.magnitune.models.RadioStation
import com.sarani.magnitune.ui.AudioPlayerSharedViewModel
import com.sarani.magnitune.ui.AuthenticationViewModel
import com.sarani.magnitune.ui.loginActivity.LogInActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class RadioStationsFragment : Fragment(), RadioStationViewHolderInteractionListener {
    private lateinit var binding: FragmentRadioStationsBinding
    private val adapter = RadioStationsRecyclerViewAdapter(this)
    private val viewModel by viewModel<RadioStationsViewModel>()
    private val sharedViewModel by activityViewModels<AudioPlayerSharedViewModel>()

    private val authenticationViewModel by viewModel<AuthenticationViewModel>()

    companion object {
        private const val showFavoritesUseCase_KEY = "showFavoritesUseCase"
        fun newInstance(showFavoritesUseCase: Boolean = false) = RadioStationsFragment().apply {
            arguments = Bundle().apply {
                putBoolean(showFavoritesUseCase_KEY, showFavoritesUseCase)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRadioStationsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (requireArguments().getBoolean(showFavoritesUseCase_KEY)) {
            binding.apply {
                llProfileOnly.visibility = View.VISIBLE
                btnLogout.setOnClickListener {
                    YoYo.with(Techniques.RubberBand).playOn(it)
                    logOut()
                }
            }
        }
        showRecyclerView(false)
        setupRecyclerView()

        if (requireArguments().getBoolean(showFavoritesUseCase_KEY)) {
            viewModel.getCurrentUserFavourites(authenticationViewModel.getCurrentUser()!!.uid)
            viewModel.currentUserFavourites.observe(viewLifecycleOwner, {
                adapter.setRadioStations(it)
                showRecyclerView(true)
            })
        } else {
            viewModel.getRadioStations(authenticationViewModel.getCurrentUser()!!.uid)
            viewModel.radioStations.observe(viewLifecycleOwner, {
                adapter.setRadioStations(it)
                showRecyclerView(true)
            })
        }

        viewModel.toast.observe(viewLifecycleOwner, {
            showToast(it)
        })
    }

    private fun setupRecyclerView() {
        binding.apply {
            rvRadioStations.layoutManager = LinearLayoutManager(
                this@RadioStationsFragment.context,
                RecyclerView.VERTICAL,
                false
            )
            this@RadioStationsFragment.adapter.setHasStableIds(true)
            rvRadioStations.adapter = this@RadioStationsFragment.adapter
        }
    }

    private fun showToast(message: String) =
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    private fun showRecyclerView(isDataLoaded: Boolean) {
        binding.apply {
            if (isDataLoaded) {
                progressBar.visibility = View.GONE
                rvRadioStations.visibility = View.VISIBLE
            } else {
                progressBar.visibility = View.VISIBLE
                rvRadioStations.visibility = View.INVISIBLE
            }
        }
    }

    override fun onClick(position: Int) {
        sharedViewModel.setRadioStation(adapter.getAllRadioStations(), position)
    }

    override fun onFavoriteIconClick(radioStation: RadioStation) {
        if (radioStation.currentUserFavourite) {
            viewModel.removeStationFromFavourites(
                radioStation,
                authenticationViewModel.getCurrentUser()!!
            )
        } else {
            viewModel.addStationToFavourite(
                radioStation,
                authenticationViewModel.getCurrentUser()!!
            )
        }
    }

    private fun logOut() {
        authenticationViewModel.signOut()
        val intent = Intent(MagniTune.application, LogInActivity::class.java)
        requireActivity().startActivity(intent)
        requireActivity().finish()
    }
}