package com.sarani.magnitune.ui.loginActivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sarani.magnitune.databinding.ActivityLogInBinding

class LogInActivity : AppCompatActivity() {

    lateinit var binding: ActivityLogInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLogInBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}