package com.sarani.magnitune.ui.audioPlayerFragment

import android.app.Application
import android.content.Context
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.wifi.WifiManager
import android.os.PowerManager
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sarani.magnitune.MagniTune
import com.sarani.magnitune.models.MediaPlayerState
import com.sarani.magnitune.models.RadioStation


class AudioPlayerViewModel(application: Application) : AndroidViewModel(application) {
    var mediaPlayerState: MediaPlayerState = MediaPlayerState.Stopped
        private set
    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast
    var currentRadioStation: RadioStation? = null
    private var mediaPlayer = MediaPlayer()

    fun handleClickOnPlayStopButton() {
        if (mediaPlayerState == MediaPlayerState.Playing) {
            toggleMediaPlayerState()
            reInitializeMediaPlayer()
        } else {
            if (currentRadioStation != null) {
                toggleMediaPlayerState()
                playRadioStation(currentRadioStation!!)
            } else _toast.postValue("None radio station selected for reproduction.")
        }
    }

    fun playRadioStation(radioStation: RadioStation) {
        mediaPlayerState = MediaPlayerState.Playing
        reInitializeMediaPlayer()
        try {
            this.currentRadioStation = radioStation

            val wifiLock: WifiManager.WifiLock =
                (getApplication<MagniTune>().applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager).run {
                    createWifiLock(WifiManager.WIFI_MODE_FULL, "MagniTune::MyWifiLockTag")
                }
            wifiLock.acquire()

            val wakeLock: PowerManager.WakeLock =
                (getApplication<MagniTune>().getSystemService(Context.POWER_SERVICE) as PowerManager).run {
                    newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MagniTune::MyWakelockTag").apply {
                    }
                }
            wakeLock.acquire()

            mediaPlayer.apply {
                setDataSource(radioStation.streamURL)
                prepareAsync()
                setOnPreparedListener {
                    it.start()
                }
            }


        } catch (e: Exception) {
            if (e.message != null)
                _toast.postValue(e.message)
        }
    }

    fun releaseMediaPlayer() {
        mediaPlayer.stop()
        mediaPlayer.release()
    }

    private fun reInitializeMediaPlayer() {
        releaseMediaPlayer()
        mediaPlayer = MediaPlayer().apply {
            setAudioAttributes(
                AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_MEDIA).build()
            )
        }
    }

    private fun toggleMediaPlayerState() {
        mediaPlayerState =
            if (mediaPlayerState == MediaPlayerState.Playing) MediaPlayerState.Stopped
            else MediaPlayerState.Playing
    }
}