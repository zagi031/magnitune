package com.sarani.magnitune.ui.loginFragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.sarani.magnitune.MagniTune
import com.sarani.magnitune.R
import com.sarani.magnitune.databinding.FragmentLogInBinding
import com.sarani.magnitune.ui.AuthenticationViewModel
import com.sarani.magnitune.ui.mainActivity.MainActivity
import com.sarani.magnitune.utils.ErrorUtil
import org.koin.androidx.viewmodel.ext.android.viewModel

class LogInFragment : Fragment() {
    private lateinit var binding: FragmentLogInBinding

    private val authenticationViewModel by viewModel<AuthenticationViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLogInBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnLogin.setOnClickListener {
            YoYo.with(Techniques.RubberBand).playOn(it)
            loginUser()
        }

        authenticationViewModel.currentUser.observe(viewLifecycleOwner, {
            val intent = Intent(MagniTune.application, MainActivity::class.java)
            requireActivity().startActivity(intent)
            requireActivity().finish()
        })

        authenticationViewModel.errorMessage.observe(viewLifecycleOwner, {
            ErrorUtil.showAlertMessage(it, requireActivity())
        })

        binding.tvSignIn.setOnClickListener {
            findNavController().navigate(LogInFragmentDirections.actionLogInFragmentToRegistrationFragment())
        }
    }

    private fun loginUser() {

        if(!validateInputFields()) {
            return
        }

        authenticationViewModel.logInUser(binding.etEmail.text.toString(), binding.etPassword.text.toString())
    }

    private fun validateInputFields(): Boolean {

        var isValid = true

        if (binding.etEmail.text.isNullOrEmpty()) {
            binding.tilEmail.error = getString(R.string.mandatory_field)
            isValid = false
        } else {
            binding.tilEmail.error = null
        }

        if (binding.etPassword.text.isNullOrEmpty()) {
            binding.tilPassword.error = getString(R.string.mandatory_field)
            isValid = false
        } else {
            binding.tilPassword.error = null
        }

        return isValid
    }
}