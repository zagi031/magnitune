package com.sarani.magnitune.ui.homeScreenFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.sarani.magnitune.R
import com.sarani.magnitune.databinding.FragmentHomeScreenBinding

class HomeScreenFragment : Fragment() {
    private lateinit var binding: FragmentHomeScreenBinding
    private lateinit var viewpagerAdapter: ViewPagerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewPager()
        setTabLayout()
    }

    private fun setViewPager() {
        this.viewpagerAdapter = ViewPagerAdapter(childFragmentManager, lifecycle)
        binding.viewpager.adapter = this.viewpagerAdapter
    }

    private fun setTabLayout() {
        TabLayoutMediator(binding.tabLayout, binding.viewpager) { tab, position ->
            tab.text = when (position) {
                0 -> resources.getString(R.string.label_allRadioStationsUseCase)
                else -> resources.getString(R.string.label_favoriteRadioStationsUseCase)
            }
        }.attach()
    }


}