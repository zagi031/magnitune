package com.sarani.magnitune.data

data class User(val uid: String, val email: String)